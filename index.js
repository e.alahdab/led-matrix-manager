const fs = require("fs");

const bodyParser = require("body-parser");
const express = require("express");

const config = require("./config");

const { scaleAndCrop } = require("./lib/ffmpeg");
const ipDisplay = require("./lib/ip-display");
const { streamConversion, play, stopAll, setVideoViewerPath, setImageViewerPath, setMatrixDefaultParameters, getNowPlaying } = require("./lib/rpi-matrix-tools");
const { determineFilename, download, setYtdlBinaryPath } = require("./lib/youtube-dl");
const { redirectOutput, outputbuffer } = require("./lib/process-helpers");
const { Playable } = require("./lib/Playable");

const { Button } = require("./lib/Button");

setYtdlBinaryPath(config.ytdlPath);
setVideoViewerPath(config.videoViewerPath);
setImageViewerPath(config.imageViewerPath);
setMatrixDefaultParameters(config.matrixDefaultParameters);

const app = express();

let playlist = [];
let playcursor = 0;

const up = new Button(16, "up");
const enter = new Button(20, "enter");
const down = new Button(21, "down");

up.on("down", function (btn, level, tick) {
	playlist = [new Playable(config.mediaPath, "clock")];
	playNext();
});
enter.on("down", function (btn, level, tick) {
	playlist = [new Playable(config.mediaPath, "ip")];
	playNext();
});
down.on("down", function (btn, level, tick) {
	playlist = [new Playable(config.mediaPath, "game-of-life")];
	playNext();
});

app.set("view engine", "pug");
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static("public"));
app.use(express.static(config.mediaPath));

app.get("/", (req, res) => {
	fs.readdir(config.mediaPath, (error, files) => {
		const pixelmatrixfiles = files
			.filter((f) => !f.startsWith("."))
			.map((f) => { return new Playable(config.mediaPath, f); });
		res.render("index", {nowPlaying: getNowPlaying(), files: pixelmatrixfiles, outputbuffer});
	});
});

app.get("/play/:file", (req, res) => {
	if (req.params.file === "all") {
		console.error("playing all in sequence currently does not work");
		/*
		fs.readdir("media", (error, files) => {
			playlist = files.filter((f) => f.endsWith(".pixelmatrix"));
			playcursor = 0;
			playNext();
		});
		*/
	} else {
		playlist = [new Playable(config.mediaPath, req.params.file)];
		playNext();
	}
	res.redirect("/");
});

app.get("/stop", (req, res) => {
	console.info("full stop requested");
	playlist = [];
	stopAll(() => {});
	res.redirect("/");
});

app.post("/download", (req, res) => {
	console.log(`Requested download of ${req.body.url}`);
	res.redirect("/");

	determineFilename(req.body.url, redirectOutput).then(async (filename) => {
		console.log("downloading %s to %s", req.body.url, filename);
		await download(req.body.url, `${config.mediaPath}/${filename}/${filename}`, redirectOutput);
		await scaleAndCrop(`${config.mediaPath}/${filename}/${filename}.mp4`, `${config.mediaPath}/${filename}/${filename}.crop.mp4`, redirectOutput);
		if (req.body.preconvert) {
			await streamConversion(`${config.mediaPath}/${filename}/${filename}.crop.mp4`, `${config.mediaPath}/${filename}/${filename}.pixelmatrix`, redirectOutput);
		}
		playlist = [new Playable(config.mediaPath, filename)];
		playNext();
	}).catch((err) => {
		console.error(err);
	});

});

app.listen(config.port, function () {
	console.log(`LED Matrix Manager now listening on port ${config.port}`);
});

ipDisplay()
	.then(() => {
		playlist = [new Playable(config.mediaPath, "ip")];
		playNext();
	});

function playNext() {
	if (playlist.length < 1) {
		console.info("playlist is empty, not playing anything");
		return;
	}
	playcursor = (playcursor + 1) % playlist.length;
	play(playlist[playcursor], redirectOutput).then((process) => {
		process.on("exit", playNext);
	});
}
