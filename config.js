module.exports = {
	// which port should this webserver listen to?
	port: 80,

	// you will probably want to use a local version, as the one from package managers is usually too old.
	ytdlPath: "./youtube-dl/yt-dlp",

	// video-viewer binary path
	videoViewerPath: "/home/pi/rpi-rgb-led-matrix/utils/video-viewer",

	// image-viewer binary path
	imageViewerPath: "/home/pi/rpi-rgb-led-matrix/utils/led-image-viewer",

	// configuration to be given to image-viewer and video-viewer. Look at
	// https://github.com/hzeller/rpi-rgb-led-matrix/tree/master/utils for details!
	matrixDefaultParameters: ["--led-cols=64", "--led-parallel=2", "--led-slowdown-gpio=4", "--led-gpio-mapping=regular", "--led-no-hardware-pulse", "--led-rows=32"],

	// directory to store media files in. No trailing slash.
	mediaPath: "media",
};
