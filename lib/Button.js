const Gpio = require('pigpio').Gpio;
const debounce = require('lodash.debounce');
const EventEmitter = require('events');

class Button extends EventEmitter {
	constructor(pin, name) {
		super();
		this.name = name;

		this.gpio = new Gpio(pin, {
			mode: Gpio.INPUT,
			pullUpDown: Gpio.PUD_UP,
			edge: Gpio.EITHER_EDGE
		});

		this.gpio.glitchFilter(10000);
		this.gpio.on('interrupt', debounce((level, tick) => {
			// console.log(`button ${this.gpio.gpio}: ${level === 0 ? "PRESSED" : "RELEASED"} (${tick})`);
			if (level) {
				this.emit("up", this, !level, tick);
			} else {
				this.emit("down", this, !level, tick);
			}
			this.emit("change", this, !level, tick);
		}, 2));

		this.callbacks = [];
	}
}

module.exports.Button = Button;
