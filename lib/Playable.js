const fs = require("fs");

class Playable {
	constructor(mediaPath, dirName) {
		this.basename = dirName;
		this.type = Playable.NONE;

		this.files = fs.readdirSync(mediaPath + "/" + this.basename);

		this.thumbnail = this.basename + "/" + this.getPreferredFile(Playable.THUMBNAIL_SUCCESSION);
		const script = this.getPreferredFile([".sh"]);
		const video = this.getPreferredFile(Playable.VIDEO_SUCCESSION);

		if (script) {
			this.type = Playable.PROGRAM;
			this.medium = mediaPath + "/" + this.basename + "/" + script;
		} else if (video) {
			this.type = Playable.VIDEO;
			this.medium = mediaPath + "/" + this.basename + "/" + video;
		} else {
			this.type = Playable.IMAGE;
			this.medium = mediaPath + "/" + this.thumbnail;
		}

		this.metafile = this.getPreferredFile([".json"]);

		if (this.metafile) {
			const data = JSON.parse(fs.readFileSync(mediaPath + "/" + this.basename + "/" + this.metafile));
			this.title = data.title;
			this.duration = data.duration_string;
			this.webpage_url = data.webpage_url;
		} else {
			this.title = this.basename;
			this.duration = "";
			this.webpage_url = "";
		}
	}

	getPreferredFile(succession) {
		for (const ending of succession) {
			for (const file of this.files) {
				if (file.endsWith(ending)) {
					return file;
				}
			}
		}
		return null;
	}

	/** whether or not this playable has a defined, automatic end. Infinite things can't be on a playlist. */
	isFinite() {
		return false;
	}
};

Playable.VIDEO_SUCCESSION = [".pixelmatrix", ".crop.webm", ".crop.mp4", ".webm", ".mp4"];
Playable.THUMBNAIL_SUCCESSION = [".webp", ".png", ".jpg"];

Playable.NONE = "none";
Playable.IMAGE = "image";
Playable.VIDEO = "video";
Playable.PROGRAM = "program";

module.exports.Playable = Playable;