const { spawn } = require("child_process");
const { Playable } = require("./Playable");

// TODO get rid of these

const tools = {
	vv: "video-viewer",
	iv: "led-image-viewer",
	vvparameters: [],
	nowPlaying: null,

	getNowPlaying: function () {
		return tools.nowPlaying;
	},

	setVideoViewerPath: function (path) {
		tools.vv = path;
	},

	setImageViewerPath: function (path) {
		tools.iv = path;
	},

	setMatrixDefaultParameters: function (parameters) {
		tools.vvparameters = parameters;
	},

	streamConversion: function (source, target, outputTarget) {
		const streamConversion = spawn("nice", ["-n", 18, tools.vv].concat(tools.vvparameters).concat(["-O", target, source]));
		outputTarget(streamConversion.stdout);
		outputTarget(streamConversion.stderr);

		return new Promise((resolve, reject) => {
			streamConversion.on("exit", (code) => {
				if (code === 0) {
					resolve();
				} else {
					reject(`Error while converting video ${source} to pixelmatrix stream ${target}`);
				}
			});
		});
	},

	currentProcess: null,

	play: function (playable, outputTarget) {
		console.log(`Attempting to play ${playable.type} ${playable.title} from ${playable.medium}`);
		return new Promise((resolve, reject) => {
			tools.stopAll(() => {
				const params = tools.vvparameters.concat(["-f", playable.medium]);
				if (tools.currentProcess) {
					console.error("THERE IS ALREADY A PROCESS RUNNING: %s", tools.currentProcess.pid);
					return;
				}
				if (playable.type === Playable.PROGRAM) {
					tools.currentProcess = spawn(playable.medium);
				} else if (playable.medium.endsWith(".mp4") || playable.medium.endsWith(".webm")) {
					tools.currentProcess = spawn(tools.vv, params);
				} else {
					tools.currentProcess = spawn(tools.iv, params);
				}

				tools.currentProcess.on("exit", (code) => {
					console.debug(`current playback ${tools.currentProcess.pid} exited in stopAll with code ${code}`);
					tools.currentProcess = null;
					tools.nowPlaying = null;
				});

				tools.nowPlaying = playable;
				console.log(`spawned new playback process ${tools.currentProcess.pid} for ${playable.medium}`);

				outputTarget(tools.currentProcess.stdout);
				outputTarget(tools.currentProcess.stderr);
				resolve(tools.currentProcess);
			});
		});
	},

	stopAll: function (callback) {
		console.debug("stopAll requested");
		if (tools.currentProcess) {
			console.debug("currently running: " + tools.currentProcess.pid);
			tools.currentProcess.on("exit", (code) => {
				callback();
			});
			tools.currentProcess.kill();
		} else {
			console.log("no process is running");
			callback();
		}
	}

};

module.exports = tools;
