const { spawn } = require("child_process");

module.exports = {
	scaleAndCrop: function (source, destination, outputTarget) {
		const ffmpeg = spawn("nice", ["-n", 18, "/usr/bin/ffmpeg", "-y", "-i", source, "-filter:v", "scale=192:32:force_original_aspect_ratio=increase,crop=192:32:0:y", "-crf", "15", "-an", destination]);
		outputTarget(ffmpeg.stdout);
		outputTarget(ffmpeg.stderr);

		return new Promise((resolve, reject) => {
			ffmpeg.on("exit", (code) => {
				if (code === 0) {
					resolve();
				} else {
					reject(`Error while converting ${source} to ${destination}`);
				}
			});
		});
	}
};
