const { spawn } = require("child_process");

module.exports = function () {
	const ipDisplay = spawn("./tools/ip-display.sh");
	return new Promise((resolve, reject) => {
		ipDisplay.on("exit", (code) => {
			if (code === 0) {
				resolve();
			} else {
				reject();
			}
		});
	});
};
