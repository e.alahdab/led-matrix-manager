const { spawn } = require("child_process");

const ytdl = {
	// override this in case you need to use a different one.
	ytdlBinary: "youtube-dl",

	setYtdlBinaryPath: function (path) {
		ytdl.ytdlBinary = path;
	},

	determineFilename: function (url, outputTarget) {
		const namePattern = "%(title)s-%(id)s";
		const filenameProcess = spawn("nice", ["-n", 18, ytdl.ytdlBinary, "--get-filename", "-o", namePattern, url]);

		if (typeof outputTarget === "function") {
			outputTarget(filenameProcess.stderr);
		}

		const p = new Promise((resolve, reject) => {
			let filename = "";
			filenameProcess.stdout.on("data", (data) => {
				filename += data.toString();
			});
			filenameProcess.on("exit", (code) => {
				if (code === 0) {
					filename = filename.trim().replace(/[/\s]/g,"_");
					resolve(filename);
				} else {
					reject(`Getting filename exited with code ${code}`);
				}
			});
		});

		return p;
	},

	download: function (url, filename, outputTarget) {
		const yt = spawn("nice", ["-n", 18, ytdl.ytdlBinary, "--write-info-json", "--write-thumbnail", "--format", "[width>192]", "--format-sort", "quality,+res,vcodec", "-o", `${filename}.%(ext)s`, url]);

		if (typeof outputTarget === "function") {
			outputTarget(yt.stdout);
			outputTarget(yt.stderr);
		}

		return new Promise((resolve, reject) => {
			yt.on("exit", (code) => {
				if (code === 0) {
					console.log("download complete");
					resolve();
				} else {
					reject(`Something went wrong while downloading ${url} to ${filename}`);
				}
			});
		});
	}
};

module.exports = ytdl;
