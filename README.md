# LED Matrix Manager

This is a tiny script to manage a HUB75 display, run by a Raspberry Pi via [hzeller's rpi-rgb-led-matrix](https://github.com/hzeller/rpi-rgb-led-matrix).

![Screenshot of the web-interface](doc/web-interface.png)

## ⚠️ Warning

The Matrix display requires superuser privileges, therefore this whole thing runs as root. This is intended to run standalone on a dedicated Raspberry Pi, so I do not believe this to be overly problematic. Please be aware that this is the case, though!


## Hardware Setup / Wiring

Wire up the LED-Matrix according to [the wiring guide from rpi-rgb-led-matrix](https://github.com/hzeller/rpi-rgb-led-matrix/blob/master/wiring.md). You could also use one of the mentioned hats.

Optionally, you can wire up three buttons from any Ground Pin to the pins GPIO16, GPIO20 and GPIO21 (pins 36, 38 and 40 on the connector).


## Software Setup

* make sure imagemagick and ffmpeg are installed: `sudo apt install imagemagick ffmpeg`
* make sure nodeJS is installed. The version from Debian 11/Raspbian packages should be NodeJS 12, and should be sufficient. `sudo apt install nodejs npm`
* Install PiGpio `sudo apt install pigpio`
* make sure the current user can use `sudo` without password. This is the default case on a Raspberry Pi.
* set up the RPI RGB LED Matrix tools:
  * clone https://github.com/hzeller/rpi-rgb-led-matrix for example to `/home/pi/rpi-rgb-led-matrix`.
  * Build the demos and the [utils](https://github.com/hzeller/rpi-rgb-led-matrix/tree/master/utils).
  * Try running its demo like this:
    `sudo ./demo -D 6 --led-cols=64 --led-chain=3`.
  * Follow the suggestions provided by the command above (cpu isolation, disabling sound ... etc.)
  * It is also a good idea to follow the suggestions at https://github.com/hzeller/rpi-rgb-led-matrix#troubleshooting
* clone this repository: `git clone https://codeberg.org/ccoenen/led-matrix-manager.git /home/pi/led-matrix-manager` (the last parameter will put it into the "right" place, but you can put it somewhere else, too)
* download [yt-dlp](https://github.com/yt-dlp/yt-dlp) binary and put it into the `youtube-dl` directory.
* copy `config.js.example` to `config.js` and check if all paths are correct and if your matrix configuration matches.
* check if the project runs as intended:
  `sudo node index.js` (Use Ctrl+C to quit)
* set up for autostart, you have (at least) two options for that:
  * via systemd:  
    Link the provided service file into the systemd directory: `sudo ln -s /home/pi/led-matrix-manager/tools/led-matrix-manager.service /etc/systemd/system/`  
    and `sudo ln -s /home/pi/led-matrix-manager/tools/ip-address-listener.service /etc/systemd/system/`  
    Reload services: `sudo systemctl daemon-reload`  
    Enable for autostart: `sudo systemctl enable led-matrix-manager` and `sudo systemctl enable ip-address-listener`  
    Start the service: `sudo systemctl start led-matrix-manager` and `sudo systemctl start ip-address-listener`  
    Check the logs if everything worked: `journalctl -u led-matrix-manager --follow`
  * via crontab:  
    add this to `/etc/crontab`:  
    `@reboot root cd /home/pi/led-matrix-manager; npm run nodemon`


## Playable things

This can play stuff that is found in the `media/` directory. Each entry is its own directory. A directory may contain multiple things, but in the end they are used in this order of preference:
- `.sh` files (note below)
- video files
- image files

images will also be used as a thumbnail for the website.


### Note on using scripts / programs... etc.

If you create a script, please be aware that this will be run with sudo/root/superuser privileges. Please make sure in your script that it cleanly exits after a `SIGTERM`. An easy way to do this, is to replace the running script with the process you're actually interested in by using `exec`.

This is an example script that I put into `media/game-of-life/game-of-life.sh`:
```sh
#!/bin/sh
exec /home/pi/rpi-rgb-led-matrix/examples-api-use/demo -D 7 --led-cols=64 --led-chain=3
```


## Development

### Easily pushing things to a Raspberry Pi

If you want to just push to your Raspberry Pi, you can enable this in your git config. Go into your `led-matrix-manager` directory and run `git config receive.denyCurrentBranch updateInstead`.

This works better with the `nodemon` way of running things, because it can auto-restart everything when project files change.


### Local Development without matrix display

In case you want to work on this without using a matrix display, you can set `videoViewerPath` and `imageViewerPath` to the provided dummy script in your `config.js`:

```js
{
	videoViewerPath: "tools/dummy-viewer.sh",
	imageViewerPath: "tools/dummy-viewer.sh",
	port: 3000,
}
```

It will just sit there and do nothing for two hours. This way, you also do not need to have root privileges.
