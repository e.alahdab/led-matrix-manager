#!/bin/sh

stdbuf -oL ip monitor | stdbuf -oL grep inet | while read line; do
	logger "new ip found in ip display-listener"
        cd /home/pi/led-matrix-manager
	tools/ip-display.sh
	sudo kill $(sudo pgrep -a led-image-viewe | grep media/ip/ip.png | awk '{print $1}')
done

exit $?

