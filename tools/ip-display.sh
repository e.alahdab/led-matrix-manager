# making sure the directory even exists
mkdir -p media/ip

# making sure this is always the current file, if any.
rm media/ip/ip.png

# generating new file
/usr/bin/convert \
 -size 64x64 \
 -background black \
 -fill darkgreen \
 -font "DejaVu-Sans-Mono" \
 -pointsize 8 label:"$(hostname) / $(ip address show | grep -v "scope host" | awk '/inet6? / {split($2,var,"/*"); print var[1]}')" \
 media/ip/ip.png

# passing on th exit code, just in case.
exit $?
