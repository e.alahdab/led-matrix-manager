#!/bin/sh
exec /home/pi/rpi-rgb-led-matrix/examples-api-use/demo -D 7 --led-slowdown-gpio=4 --led-gpio-mapping=regular --led-no-hardware-pulse --led-parallel=2 --led-cols=64 --led-rows=32
