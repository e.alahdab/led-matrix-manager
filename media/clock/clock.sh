#!/bin/sh
exec /home/pi/rpi-rgb-led-matrix/examples-api-use/clock -f /home/pi/rpi-rgb-led-matrix/fonts/7x13.bdf --led-slowdown-gpio=4 --led-gpio-mapping=regular --led-no-hardware-pulse --led-parallel=2 --led-cols=64 --led-rows=32 -d "%A" -d "%H:%M:%S"
